﻿using MovieReview.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    public class MovieRatingRepository : IMovieRatingRepository
    {
        public readonly ApplicationDbContext appDbContext;

        public MovieRatingRepository(ApplicationDbContext appDbContext)
        {

            this.appDbContext = appDbContext;

        }

        public void AddMovieReview(MovieRating movieReview)
        {
            appDbContext.MovieRating.Add(movieReview);
            appDbContext.SaveChanges();
        }

        public IEnumerable<MovieRating> GetReviewsForMovie(int movieId)
        {
            return appDbContext.MovieRating.Where(m => m.MovieId == movieId);
        }
    }
}
