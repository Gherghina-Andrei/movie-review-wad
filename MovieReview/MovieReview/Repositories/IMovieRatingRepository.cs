﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    interface IMovieRatingRepository
    {

        void AddMovieReview(MovieRating movieReview);

        IEnumerable<MovieRating> GetReviewsForMovie(int movieId);



    }
}
