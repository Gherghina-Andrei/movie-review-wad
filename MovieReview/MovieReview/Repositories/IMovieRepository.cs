﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    interface IMovieRepository
    {

        IEnumerable<Movie> getAllMovies();

        Movie getMovieById(int movieId);


    }
}
