﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieReview.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    public class MovieRepository : IMovieRepository
    {

        private readonly ApplicationDbContext appDbContext;

        public MovieRepository(ApplicationDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public IEnumerable<Movie> getAllMovies()
        {
            return appDbContext.Movie;
        }

        public Movie getMovieById(int movieId)
        {
            return appDbContext.Movie.FirstOrDefault(movie => movie.MovieId == movieId);
        }

       
    }
}
