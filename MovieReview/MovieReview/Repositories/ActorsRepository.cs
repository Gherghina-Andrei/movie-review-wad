﻿using MovieReview.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    public class ActorsRepository : IActorRepository
    {
        private readonly ApplicationDbContext appDbContext;

        public ActorsRepository(ApplicationDbContext appDbContext)
        {
            this.appDbContext = appDbContext;

        }

        public Actor GetActorById(int actorId)
        {
            return appDbContext.Actor.FirstOrDefault(actor => actor.ActorId == actorId);
        }

        public IEnumerable<Actor> GetAllActors()
        {
            return appDbContext.Actor;
        }


    }
}
