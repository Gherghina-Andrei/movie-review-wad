﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    interface IActorRepository
    {
        IEnumerable<Actor> GetAllActors();

        Actor GetActorById(int actorId);

    }
}
