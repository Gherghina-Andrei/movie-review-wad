﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MovieReview.Models;

namespace MovieReview.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ActorMovie>()
                .HasKey(t => new { t.MovieId, t.ActorId });

            builder.Entity<ActorMovie>()
                .HasOne(p => p.Movie)
                .WithMany(x => x.Actors)
                .HasForeignKey(y => y.MovieId);

            builder.Entity<ActorMovie>()
               .HasOne(p => p.Actor)
               .WithMany(x => x.Movies)
               .HasForeignKey(y => y.ActorId);

        }

        public DbSet<Movie> Movie { get; set; }

        public DbSet<Actor> Actor { get; set; }

        public DbSet<Director> Director { get; set; }

        public DbSet<MovieRating> MovieRating { get; set; }

        public DbSet<ActorMovie> ActorMovie { get; set; }
    }
}
