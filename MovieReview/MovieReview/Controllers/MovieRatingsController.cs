﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieReview.Data;
using MovieReview.Models;

namespace MovieReview.Controllers
{
    public class MovieRatingsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public MovieRatingsController(ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: MovieRatings
        public async Task<IActionResult> Index()
        {
            return View(await _context.MovieRating.ToListAsync());
        }

        public ActionResult SetRating(int MovieId,decimal rank)
        {
            MovieRating rating = new MovieRating();
            rating.Rating = rank;
            rating.MovieId = MovieId;
            rating.UserId = _userManager.GetUserId(User);

            Movie movie = _context.Movie.FirstOrDefault(m => m.MovieId == MovieId);
            rating.Movie = movie;
            movie.MovieRatings.Add(rating);
        

            _context.MovieRating.Add(rating);
            _context.SaveChanges();
            _context.Movie.Update(movie);
            _context.SaveChanges();

            return RedirectToAction("Details", "Movies", new { id = MovieId });
        }

        // GET: MovieRatings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieRating = await _context.MovieRating
                .FirstOrDefaultAsync(m => m.MovieRatingId == id);
            if (movieRating == null)
            {
                return NotFound();
            }

            return View(movieRating);
        }

        // GET: MovieRatings/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MovieRatings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MovieRatingId,Rating")] MovieRating movieRating)
        {
            if (ModelState.IsValid)
            {
                _context.Add(movieRating);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(movieRating);
        }

        // GET: MovieRatings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieRating = await _context.MovieRating.FindAsync(id);
            if (movieRating == null)
            {
                return NotFound();
            }
            return View(movieRating);
        }

        // POST: MovieRatings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MovieRatingId,Rating")] MovieRating movieRating)
        {
            if (id != movieRating.MovieRatingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(movieRating);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieRatingExists(movieRating.MovieRatingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(movieRating);
        }

        // GET: MovieRatings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieRating = await _context.MovieRating
                .FirstOrDefaultAsync(m => m.MovieRatingId == id);
            if (movieRating == null)
            {
                return NotFound();
            }

            return View(movieRating);
        }

        // POST: MovieRatings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movieRating = await _context.MovieRating.FindAsync(id);
            _context.MovieRating.Remove(movieRating);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MovieRatingExists(int id)
        {
            return _context.MovieRating.Any(e => e.MovieRatingId == id);
        }
    }
}
