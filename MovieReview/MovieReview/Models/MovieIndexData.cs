﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    public class MovieIndexData
    {
        public IEnumerable<Movie> Movies { get; set; }

        public IEnumerable<ActorMovie> Actors { get; set; }

        public IEnumerable<MovieRating> MovieRatings { get; set; }

    }
}
