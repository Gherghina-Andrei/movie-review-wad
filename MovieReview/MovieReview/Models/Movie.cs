﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MovieReview.Models
{
    public class Movie
    {
        public int MovieId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        [DisplayName("Upload Image")]
        public string ImagePath { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Year { get; set; }

        public string Genre { get; set; }

        public ICollection<MovieRating> MovieRatings { get; set; }

        [NotMapped]
        public decimal OverallRating
        {
            get
            {
                if (MovieRatings.Count > 0)
                {
                    return (MovieRatings.Average(x => x.Rating));
                }

                return (10);
            }
          
            
        }
        
        public int DirectorId { get; set; }

        public Director Director { get; set; }

        public ICollection<ActorMovie> Actors { get; set; }

    }
}
