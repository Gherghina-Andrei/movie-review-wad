﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MovieReview.Models
{
    public class MovieRating
    {

        public int MovieRatingId { get; set; }

        public int MovieId { get; set; }

        public Movie Movie { get; set; }

        public string UserId { get; set; }

        [DisplayName("Rating")]
        [Range(1,10)]
        public decimal Rating { get; set; }

    }
}
