﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    public class Director
    {

        public int DirectorId { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

    
    }
}
