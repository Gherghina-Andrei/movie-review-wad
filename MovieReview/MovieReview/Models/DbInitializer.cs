﻿using MovieReview.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReview.Models
{
    public static class DbInitializer
    {
        
        public static void Seed(ApplicationDbContext context)
        {
            

                Actor actor1 = new Actor();
                actor1.ActorId = 1;
                actor1.Age = 31;
                actor1.Name = "Hugh Jacman";
                context.Actor.Add(actor1);

                Actor actor2 = new Actor();
                actor2.ActorId = 2;
                actor2.Age = 47;
                actor2.Name = "Jim Carey";
                context.Actor.Add(actor2);


                Actor actor3 = new Actor();
                actor3.ActorId = 3;
                actor3.Age = 50;
                actor3.Name = "Samuel L. Jackson";
                context.Actor.Add(actor3);

                Actor actor4 = new Actor();
                actor4.ActorId = 4;
                actor4.Age = 53;
                actor4.Name = "Bruce Willis";
                context.Actor.Add(actor4);

                Actor actor5 = new Actor();
                actor5.ActorId = 5;
                actor5.Age = 33;
                actor5.Name = "Kit Harington";
                context.Actor.Add(actor5);

                Actor actor6 = new Actor();
                actor6.ActorId = 6;
                actor6.Age = 35;
                actor6.Name = "Jason Momoa";
                context.Actor.Add(actor6);

                MovieRating rating1 = new MovieRating();
                rating1.MovieRatingId = 1;
                rating1.Rating = 8;
                
                context.MovieRating.Add(rating1);

                MovieRating rating2 = new MovieRating();
                rating2.MovieRatingId = 2;
                rating2.Rating = 5;
                
                context.MovieRating.Add(rating2);

                MovieRating rating3 = new MovieRating();
                rating3.MovieRatingId = 3;
                rating3.Rating = 10;
                
                context.MovieRating.Add(rating3);

                MovieRating rating4 = new MovieRating();
                rating4.MovieRatingId = 4;
                rating4.Rating = 7;
                
                context.MovieRating.Add(rating4);

                Director director1 = new Director();
                director1.DirectorId = 1;
                director1.Age = 47;
                director1.Name = "James Cameron";
                context.Director.Add(director1);

                Director director2 = new Director();
                director2.DirectorId = 2;
                director2.Age = 48;
                director2.Name = "James Gunn";
                context.Director.Add(director2);

                Director director3 = new Director();
                director3.DirectorId = 3;
                director3.Age = 47;
                director3.Name = "James Franco";
                context.Director.Add(director3);

                Director director4 = new Director();
                director4.DirectorId = 4;
                director4.Age = 47;
                director4.Name = "James Wan";
                context.Director.Add(director4);

                Movie movie1 = new Movie();
                movie1.MovieId = 1;
                movie1.Title = "Logan";
                movie1.Description = "In 2029 the mutant population has shrunken significantly due to genetically modified plants designed to reduce mutant powers and the X - Men have " +
                "disbanded. Logan, whose power to self-heal is dwindling, has surrendered himself to alcohol and now earns a living as a chauffeur. He takes care of the ailing old Professor X whom he keeps" +
                " hidden away. One day, a female stranger asks Logan to drive a girl named Laura to the Canadian border. At first he refuses, but the Professor has been waiting for a long time for her to " +
                "appear. Laura possesses an extraordinary fighting prowess and is in many ways like Wolverine. She is pursued by sinister figures working for a powerful corporation; this is because they " +
                "made her, with Logan's DNA. A decrepit Logan is forced to ask himself if he can or even wants to put his remaining powers to good use. It would appear that in the near-future, the times in " +
                "which they were able put the world to rights with razor sharp claws and telepathic powers are now over. ";
                movie1.Year = new DateTime(2017, 04, 30); ;
                movie1.Genre = "Action";
             
                context.Movie.Add(movie1);


                context.SaveChanges();



/*
                new Movie
                {
                    Title = "Logan",
                    Description = "n 2029 the mutant population has shrunken significantly due to genetically modified plants designed to reduce mutant powers and the X-Men have " +
                "disbanded. Logan, whose power to self-heal is dwindling, has surrendered himself to alcohol and now earns a living as a chauffeur. He takes care of the ailing old Professor X whom he keeps" +
                " hidden away. One day, a female stranger asks Logan to drive a girl named Laura to the Canadian border. At first he refuses, but the Professor has been waiting for a long time for her to " +
                "appear. Laura possesses an extraordinary fighting prowess and is in many ways like Wolverine. She is pursued by sinister figures working for a powerful corporation; this is because they " +
                "made her, with Logan's DNA. A decrepit Logan is forced to ask himself if he can or even wants to put his remaining powers to good use. It would appear that in the near-future, the times in " +
                "which they were able put the world to rights with razor sharp claws and telepathic powers are now over. ",
                    Genre = "Action",
                    /* 
                      Actors = new List<Actor>
                      {
                          new Actor{Name = "Hugh Jackman",Role = "Wolverine"},
                          new Actor{Name = "Patrick Stewart",Role = "Charles Xavier" }

                      },
                      Director = new Director
                      {
                          Name = "James Mangold",
                          Age = 43,

                      }
                };



                new Movie
                {
                    Title = "Guardians of the Galaxy",
                    Description = "After stealing a mysterious orb in the far reaches of outer space," +
                    "Peter Quill from Earth is now the main target of a manhunt led by the villain known as Ronan the Accuser.To help fight Ronan and his team and save the galaxy from his power," +
                    "Quill creates a team of space heroes known as the Guardians of the Galaxy to save the galaxy.",
                    Genre = "Adventure",

                    /*  Actors = new List<Actor>
                     {
                         new Actor{Name = "Chris Pratt",Role = "Peter Quill"},
                         new Actor{Name = "Dave Bautista",Role = "Drax" }

                     },
                      Director = new Director
                      {
                          Name = "James Gunn",
                          Age = 47,

                      }
                };*/


            }

        }

 
    
}
